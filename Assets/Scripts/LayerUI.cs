﻿using UnityEngine;

public class LayerUI : MonoBehaviour
{
    #region VARIABLES

    public Animator explodeAnimation;

    //public List<GameObject> layers;
    private Animator pageAnimator;

    private int currentPage;
    private bool isOpenPage;

    private InputControl inputControl;

    #endregion

    #region Unity_Methods

    // Start is called before the first frame update
    void Start()
    {
        inputControl = InputControl.S;

        pageAnimator = GetComponent<Animator>();

        currentPage = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputControl == null)
            inputControl = InputControl.S;

        
        if(isOpenPage)
        {
            if (inputControl.SwipeDown)
            {
                isOpenPage = false;
                //pageAnimator.SetInteger("contentID", currentPage);
                pageAnimator.SetBool("content", false);
            }
        }
        else
        {
            if (inputControl.SwipeUp)
            {
                if (currentPage > 0)
                {
                    isOpenPage = true;
                    //pageAnimator.SetInteger("contentID", currentPage);
                    pageAnimator.SetBool("content", true);
                }
            }
            else if (inputControl.SwipeLeft)
            {
                currentPage++;
                if (currentPage > 4) currentPage = 4;
                pageAnimator.SetInteger("pageID", currentPage);
                explodeAnimation.SetInteger("hideLayerID", currentPage);
            }
            else if (inputControl.SwipeRight)
            {
                currentPage--;
                if (currentPage < 0) currentPage = 0;
                pageAnimator.SetInteger("pageID", currentPage);
                explodeAnimation.SetInteger("hideLayerID", currentPage);
            }
        }
        
        
        //else 

        //pageAnimator.SetBool("LeftSwipe", false);
        //if (currentPage == 0)
        //{
        //    if (inputControl.SwipeLeft)
        //    {
        //        //pageAnimator.SetBool("LeftSwipe", true);
        //        pageAnimator.SetInteger("pageID", currentPage);
        //        currentPage = 1;
        //    }
        //}
        //else if (currentPage == 1)
        //{
        //    if (inputControl.SwipeLeft)
        //    {
        //        pageAnimator.SetBool("LeftSwipe", false);
        //        currentPage = 2;
        //    }
        //}
        //else if (currentPage == 2)
        //{
        //    //if (inputControl.SwipeLeft)
        //    //{
        //    //    pageAnimator.SetBool("LeftSwipe", false);
        //    //}
        //}
        //else if (currentPage == 3)
        //{
        //    //if (inputControl.SwipeLeft)
        //    //{
        //    //    pageAnimator.SetBool("LeftSwipe", false);
        //    //}
        //}

    }

    #endregion
}

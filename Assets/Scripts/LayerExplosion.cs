﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class LayerExplosion : MonoBehaviour
{
    #region VARIABLES

    public GameObject explodeLayers;

    public GameObject layersPanel;

    private ARTrackedImageManager trackedImageManager;
    private Animator explodeAnimation;
    private Animator layersAnimation;

    private InputControl inputControl;

    #endregion

    #region Unity_Methods

    void Awake()
    {
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        explodeAnimation = explodeLayers.GetComponent<Animator>();
        layersAnimation = layersPanel.GetComponent<Animator>();

        inputControl = InputControl.S;
    }

    public void OnEnable()
    {
        trackedImageManager.trackedImagesChanged += OnImageTracked;
    }

    public void OnDisable()
    {
        trackedImageManager.trackedImagesChanged -= OnImageTracked;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputControl == null)
            inputControl = InputControl.S;

        //if (inputControl.SwipeUp)
        //{
        //    explodeAnimation.SetTrigger("Explode");
        //    //layersAnimation.SetTrigger("Swap");
        //}
        //else if (inputControl.SwipeDown)
        //{
        //    explodeAnimation.SetTrigger("Wrap");
        //    //layersAnimation.SetTrigger("Wrap");
        //}
    }

    #endregion

    #region METHODS

    //GameObject newObj;
    public void OnImageTracked(ARTrackedImagesChangedEventArgs args)
    {
        foreach (ARTrackedImage addedImage in args.added)
        {
            string name = addedImage.referenceImage.name;
            //newObj = Instantiate(layerObj, Vector3.zero, Quaternion.identity);
            explodeLayers.transform.position = addedImage.transform.position;
            explodeLayers.transform.rotation = addedImage.transform.rotation;

            explodeLayers.transform.localScale = new Vector3(addedImage.referenceImage.size.x, 0.005f, addedImage.referenceImage.size.y);
            //explodeLayers.transform.Rotate(0.0f, 0.0f, 0.0f);
            explodeLayers.SetActive(true);
            explodeAnimation.SetTrigger("Explode");
            layersAnimation.SetBool("start", true);
        }
        foreach (ARTrackedImage updatedImage in args.updated)
        {
            string name = updatedImage.referenceImage.name;
            explodeLayers.transform.position = updatedImage.transform.position;
            explodeLayers.transform.rotation = updatedImage.transform.rotation;
        }
    }

    #endregion

}

﻿using UnityEngine;

public class InputControl : MonoBehaviour
{
    #region VARIABLES

    [SerializeField]
    private Camera arCamera;

    public static InputControl S;

    private bool swipeLeft, swipeRight, swipeUp, swipeDown;
    private bool isDraging = false;

    private Vector2 startTouch, swipeDelta;

    private float timeElapsed, scalarVelocity;

    #endregion

    #region Unity_Methods

    private void Awake()
    {
        S = this;
    }

    void Update()
    {
        swipeRight = swipeLeft = swipeUp = swipeDown = false;

        if (Input.touches.Length > 0)
        {
            Touch firstTouch = Input.GetTouch(0);
            if (firstTouch.phase == TouchPhase.Began)
            {
                // Swipe 
                //Ray ray = arCamera.ScreenPointToRay(firstTouch.position);
                //RaycastHit hitObj;

                //if (EventSystem.current.IsPointerOverGameObject(firstTouch.fingerId))
                //{
                //    //logTxt.text = "Something is over, Idiot!";
                //    isCanvas = true;
                //}
                //else if (Physics.Raycast(ray, out hitObj))
                //{
                //    //logTxt.text = "You are welcome";
                //    flavor = hitObj.transform.GetComponent<FlavourController>();
                //    bottleSilhouette = hitObj.transform.GetComponent<BottleSilhouette>();
                //    if (flavor != null)
                //    {
                //        isFlavour = true;
                //    }
                //    if (bottleSilhouette != null)
                //    {
                //        logTxt.text = "Is Silhouette";
                //        isSilhouette = true;
                //    }
                //}
                isDraging = true;
                startTouch = firstTouch.position;
                timeElapsed = scalarVelocity = 0.0f;
            }
            else if (firstTouch.phase == TouchPhase.Ended || firstTouch.phase == TouchPhase.Canceled)
            {
                Reset();
            }

            //if (isSilhouette && Input.touchCount == 2)
            //{
            //    isPinch = true;
            //    isDraging = false;
            //    logTxt.text = "Is Pinch";
            //    Touch secondTouch = Input.GetTouch(1);

            //    Vector2 firstTouchPrevPos = firstTouchPos - firstTouch.deltaPosition;
            //    Vector2 secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            //    float prevMagnitude = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            //    float currentMagnitude = (firstTouchPos - secondTouch.position).magnitude;

            //    pinchDistance = currentMagnitude - prevMagnitude;
            //}

        }

        // Calculate distance.
        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            timeElapsed += Time.deltaTime;
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
        }

        if (swipeDelta.magnitude > 125)
        {
            scalarVelocity = swipeDelta.magnitude / timeElapsed;
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                // Left or Right.
                if (x < 0)
                    swipeLeft = true;
                else
                    swipeRight = true;
            }
            else
            {
                // Up or Down.
                if (y < 0)
                    swipeDown = true;
                else
                    swipeUp = true;
            }
            Reset();
        }
    }
    
    #endregion

    #region METHODS

    public void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = /*isSilhouette =*/ false;
    }

    #endregion

    #region Poperties

    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool SwipeDown { get { return swipeDown; } }

    #endregion

}
